---
layout: markdown_page
title: "Business Operations - Customer Lifecycle"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Demand waterfall

A demand waterfall is a term used to track the different milestones prospective customers go through as they learn about GitLab and interact with our sales and marketing teams. Each subsequent milestone is a subset of the previous milestone, and represents a progression from not knowing GitLab to being a customer (and fan) of GitLab.

At the highest level of abstraction, we use the terms `lead` `opportunity` and `customer` to represent a person's progression towards becoming a customer of GitLab. Those three terms also correspond to record types in salesforce.com

Lead => Opportunity => Customer

However, there are more granular steps within the above milestones that are used to track the above process with more precision. They are tracked as follows:

| Funnel stage | Record Type | Status or Stage |
|---------------|--|----------------------|-------------------|
| Raw | [Lead or Contact] | Raw |
| Inquiry | [Lead or Contact] | Inquiry |
| Marketo Qualified Lead | [Lead or Contact] |  MQL |
| Accepted Lead | [Lead or Contact] |  Accepted |
| Qualifying | [Lead or Contact] |  Qualifying |
| Pending Acceptance | [Opportunity] | 0 - Pending Acceptance |
| [Sales Accepted Opportunity] | [Opportunity] | 1 - Discovery |
| Sales Qualified Opportunity | [Opportunity] | 2 Scoping - 5 Negotiating |
| Customer | [Opportunity] | 6-Closed Won |

For the definition of the stage please click the record type link.

When going from Qualifying to Qualified Lead the lead is duplicated to an opportunity, and the lead is set to qualified and not being used anymore.


[Lead or Contact]: /handbook/business-ops/database-management/#lead--contact-statuses
[Opportunity]: /handbook/sales/#opportunity-stages
[Sales Accepted Opportunity]: /handbook/marketing/marketing-sales-development/sdr/#criteria-for-sales-accepted-opportunity-sao
