---
layout: markdown_page
title: "Customer Success"
---
# Welcome to the Customer Success Handbook
{:.no_toc}

The Customer Success team is part of the [GitLab Sales](/handbook/sales) team who partners with our large and strategic customers to deliver value throughout their journey with GitLab.  

* [Solutions Architects](/handbook/customer-success/solutions-architects) are responsible for actively driving and managing the technology evaluation and validation stages of the sales process.
* [Implementation Specialists](/handbook/customer-success/professional-services) act as the technical representative leading the direct interaction with the customer’s personnel and project teams by rolling out best practices.
* [Technical Account Managers](/handbook/customer-success/tam) the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab.

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission Statement
To deliver value to all customers by engaging in a consistent, repeatable, scalable way across defined segments so that customers see the value in their investment with GitLab, and we retain and drive growth in our within our enterprise customers. 

* The mission of the solutions architect team is to provide these customers with experience in order to:
    * Accelerate initial customer value
    * Maximize long-term, sustainable customer value
    * Improve overall customer satisfaction & referenceability
    * Maximize the total value of the customer to GitLab

    
## Team Initiatives
Our large and strategic customers are in need of an ongoing partnership that combines expert guidance with flexibility and adaptability to support their adoption and continuous improvement initiatives.  

These customers expect that partner to provide a streamlined, consistent and fully coordinated experience that encompasses the full span of their needs as well as the fully lifecycle of the relationship.
Need to focus on 3 main areas in order to grow in our existing accounts as well as land large and strategic:

1. Awareness
2. Adoption
3. Usage


### Initiative: Awareness
Opportunity to improve the overall awareness of GitLab in order to promote and evangelize our brand and solution in a meaningful way to provide big business impact to our customers so that they believe in our vision and strategy.

### Initiative: Adoption
Ensuring paying customers are successful in their onboarding in order to gain adoption and get the most out of our platform and remain happy, paying GitLabers and brand advocates.

### Initiative: Usage
Collecting and making use of customer data and insights is key to customer success.  It’s important to do more with data and when necessary share back with customers, which in turn helps and encourages our customers to improve and drive adoption.

## Customer Success Functions
**Solutions Architects**
**Solution Architects** are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the technical solutions while also understanding the business challenges the customer is trying to overcome. Solution Architects team up with our strategic account leaders to help sell into our large and strategic accounts.

* **Sales Team**: [When and How to Engage a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect)
* **Other GitLabbers**: You can reach out on our Slack channel [#solutions_architects](https://gitlab.slack.com/messages/C5D346V08/) or e-mail [sa-team@gitlab.com](mailto:sa-team@gitlab.com)

**Professional Services**
**Implementation Specialists** have the goal to optimize your organization's adoption of GitLab through our implementation services, designed to enable other necessary systems in your environment as they are used by your teams moving code from idea to production.

* [GitLab Professional Services](/handbook/customer-success/professional-services)

**Technical Account Manager**
**Technical Account Manager** help our Strategic customers continue to drive adoption of cloud-native SDLC best practices throughout their tenure as a GitLab customer.  Focused on the journey to Complete DevOps, TAMs will be with customers even after implementation to continue to advise and adapt to customers changing needs.

## Other Resources
### Customer Success resource links outside handbook

* [Solutions Architects Onboarding Bootcamp](/handbook/customer-success/solutions-architects-onboarding-bootcamp/)
* [HealthCheck](https://docs.google.com/document/d/1aHA3W2FsHUApnz2XVtJoyhpcGYy6bgOHoRi4ArXnF0o/edit) Internal Only
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [GitLab University](https://docs.gitlab.com/ce/university/)
* [Our Support Handbook](/handbook/support/)
* [GitLab Hosted](https://about.gitlab.com/gitlab-hosted/)
* [Workflow SA Demo Scenarios](https://docs.google.com/document/d/1kSVUNM4u6KI8M9FxoyiUbHEHAHIi34iiY25NhMxLucc/edit) Internal Only

### Other Sales Topics

* [Sales HandBook](/handbook/sales/)
* [Sales Standard Operating Procedures](/handbook/sales/sop/)
* [Sales Operations](/handbook/sales/salesops/)
* [Sales Skills Best Practices](/handbook/sales-training/)
* [Sales Discovery Questions](/handbook/sales-qualification-questions/)
* [EE Product Qualification Questions](/handbook/EE-Product-Qualification-Questions/)
* [GitLab Positioning](/handbook/positioning-faq/)
* [FAQ from prospects](/handbook/sales-faq-from-prospects/)
* [CLient Use Cases](/handbook/use-cases/)
* [POC Template](/handbook/sales/POC/)
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24))
* [Sales Demo](/handbook/sales/demo/)
* [Idea to Production Demo](/handbook/product/i2p-demo)
* [Sales Development Team Handbook](/handbook/sales/sdr)
* [Who to go to to ask Questions or Give Feedback on a GitLab feature](/handbook/product/#who-to-talk-to-for-what)
* [CEO Preferences when speaking with prospects and customers](/handbook/people-operations/ceo-preferences/#sales-meetings)

### Customer On-boarding Process

This content has moved to the [Account Management Handbook](/handbook/account-management/)

### Customer Success & Market Segmentation
For definitions of the account size, refer to [market segmentation](/handbook/sales#market-segmentation) which is based on the _Potential EE Users_ field on the account.

- Strategic: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Large: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Mid-Market: Account Executive closes deal and transfers to Account Manager to handle renewals and growth. The Account Manager records the Previous Account Owner on the Opportunity Team in any deals while the account is in the New Customer period within the first 12 months including the first annual renewal.
- SMB: Web Portal with Sales Admin oversight. 